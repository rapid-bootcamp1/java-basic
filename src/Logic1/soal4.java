package Logic1;

public class soal4 {
    public static void main(String[] args) {
        int nilai =9;
        soal04A(nilai);
        soal04B(nilai);
        soal04C(nilai);
        soal04D(nilai);
        soal04E(nilai);
    }
    public static void soal04A(int n){
        int[] deret = new int[n];
        // 1  1  2  3  5  8 13  21
        // 0  1  2  3  4  5  6  7
        System.out.print("Hasil 4A: ");
        for (int i = 0; i < deret.length; i++){
            if(i ==0 || i ==1){
                // deret ke i => 0, 1
                deret[i] = 1;
                System.out.print(deret[i] +"\t");
            }else {
                deret[i] = deret[i-1] + deret[i-2];
                System.out.print(deret[i] +"\t");
            }
        }
    }

    public static void soal04B(int n){
        int[] deret = new int[n];
        // 1  1  2  3  5  8 13  21
        // 0  1  2  3  4  5  6  7
        System.out.print("Hasil 4A: ");
        for (int i = 0; i < deret.length; i++){
            if(i ==0 || i ==1){
                // deret ke i => 0, 1
                deret[i] = 1;
                System.out.print(deret[i] +"\t");
            }else {
                deret[i] = deret[i-1] + deret[i-2];
                System.out.print(deret[i] +"\t");
            }
        }
    }

    public static void soal04C(int n){
        int[] deret = new int[n];
        // 1  1  2  3  5  8 13  21
        // 0  1  2  3  4  5  6  7
        System.out.print("Hasil 4A: ");
        for (int i = 0; i < deret.length; i++){
            if(i ==0 || i ==1){
                // deret ke i => 0, 1
                deret[i] = 1;
                System.out.print(deret[i] +"\t");
            }else {
                deret[i] = deret[i-1] + deret[i-2];
                System.out.print(deret[i] +"\t");
            }
        }
    }

    public static void soal04D(int n){
        int[] deret = new int[n];
        // 1  1  2  3  5  8 13  21
        // 0  1  2  3  4  5  6  7
        System.out.print("Hasil 4A: ");
        for (int i = 0; i < deret.length; i++){
            if(i ==0 || i ==1){
                // deret ke i => 0, 1
                deret[i] = 1;
                System.out.print(deret[i] +"\t");
            }else {
                deret[i] = deret[i-1] + deret[i-2];
                System.out.print(deret[i] +"\t");
            }
        }
    }

    public static void soal04E(int n){
        int[] deret = new int[n];
        // 1  1  2  3  5  8 13  21
        // 0  1  2  3  4  5  6  7
        System.out.print("Hasil 4A: ");
        for (int i = 0; i < deret.length; i++){
            if(i ==0 || i ==1){
                // deret ke i => 0, 1
                deret[i] = 1;
                System.out.print(deret[i] +"\t");
            }else {
                deret[i] = deret[i-1] + deret[i-2];
                System.out.print(deret[i] +"\t");
            }
        }
    }
}
